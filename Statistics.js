import React from "react";
import { Box, Flex, Text, Button } from "native-base";
import { useState } from 'react';
 
export default function Statistics() {
    const [sampleNumber, setSampleNumber] = useState(null);
    
    const getRandomNumber = () => {
        setSampleNumber( Math.random().toString().substr(2,3) );
    }
    
    return (
        <Flex
            backgroundColor='gray.800'
            direction='column'
            paddingX='1.5'
            height='100%'
            justifyContent='center'
            alignItems='center'
        >
            <Box
                backgroundColor='black'
                width='50%'
                height='12%'
                justifyContent='center'
                alignItems='center'
                marginBottom='10%'
            >
                <Text
                    color='white'
                    fontSize='5xl'
                >{sampleNumber}</Text>
            </Box>
            
            <Button
                width='30%'
                backgroundColor='#2B5F75'
                _text={{fontWeight:'bold', fontSize:'xl'}}
                onPress={() =>getRandomNumber()}
            >Spin</Button>
        </Flex>
    );
}
