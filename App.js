import React from "react";
import { NativeBaseProvider} from "native-base";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { SafeAreaView } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Header from "./Header";
import Table from "./Table";
import CheckTicket from "./CheckTicket";
import BottomNavigation from "./BottomNavigation";
import { enableScreens } from "react-native-screens";
import Statistics from "./Statistics";

enableScreens()
const Stack = createNativeStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
    > 
      <Stack.Screen name='LotteryResults' component={Table} />
      <Stack.Screen name='CheckTicket' component={CheckTicket} />
      <Stack.Screen name='Statistics' component={Statistics} />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <NativeBaseProvider>
        <SafeAreaProvider>
        <SafeAreaView>
          <Header/>
        </SafeAreaView>
          <MyStack />
        <SafeAreaView>
          <BottomNavigation />
        </SafeAreaView>
        </SafeAreaProvider>
      </NativeBaseProvider>
    </NavigationContainer>
  );
}