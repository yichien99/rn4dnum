import React, { useState } from "react";
import { Box, Flex } from "native-base";

const TableHeader = ({children, twoColumn, leftTitle, rightTitle, title}) => {
    

    return (
        <>
        {twoColumn ? (
            <Flex 
                direction="row"
                background='#3C3881'
            >
                    <Box 
                        width='50%'
                        _text={{
                            color: 'white',
                            textAlign: 'center',
                            fontSize: "xl",
                            fontWeight: "bold",
                        }}
                    >{leftTitle}</Box>
                    <Box 
                        width='50%'
                        _text={{
                            color: 'white',
                            textAlign: 'center',
                            fontSize: "xl",
                            fontWeight: "bold",
                        }}
                    >{rightTitle}</Box>
                    {children}
            </Flex>
        ) : (
            <Flex 
                direction="row"
                background='#3C3881' 
            >
                    <Box 
                        width='100%'
                        _text={{
                            color: 'white',
                            textAlign: 'center',
                            fontSize: "xl",
                            fontWeight: "bold",
                        }}
                    >{title}</Box>
                    {children}
            </Flex>
        )}
        </>
    )
  };
 
export default TableHeader;