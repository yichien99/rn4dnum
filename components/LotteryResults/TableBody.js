import React from "react";
import { Box, Flex } from "native-base";

const TableBody = ({width,twoColumn, leftNum, rightNum, number, alignLeftRight, bold, largeFont}) => {
    return (
        <>
        {twoColumn ? (
            <Flex 
                direction="row"
                marginBottom='3'
                width={width}
                background='white' 
                paddingY='2'
            >
                    <Box 
                        width='50%'
                        _text={{
                            color: 'black',
                            textAlign: alignLeftRight ? 'right' : 'center',
                            fontSize: largeFont ? '25': '20',
                            fontWeight: bold ? '700': '600',
                        }}
                        paddingRight='3'
                    >{leftNum}</Box>
                    <Box
                        width='50%'
                        _text={{
                            color: 'black',
                            textAlign: alignLeftRight ? 'left' : 'center',
                            fontSize: largeFont ? '25': '20',
                            fontWeight: bold ? '700': '600',
                        }}
                        paddingLeft= '2'
                    >{rightNum}</Box>
            </Flex>
        ) : (
            <Box 
                marginBottom='3'
                background='white' 
                paddingY='2'
                paddingX='5'
                width='100%'
            >
                    <Box 
                        flexDirection='row'
                        flexWrap='wrap'
                        justifyContent='center'
                        width='100%'
                        
                    >{number}</Box>
            </Box>
        )}
        </>
    )
  };
 
export default TableBody;