import React from "react";
import { Flex, Text, DeleteIcon, Box} from "native-base";
 
const TableCheck= ({date, num, result, action, resultColor}) =>{
  // const [show, setShow] = React.useState(false);
  // const handleClick = () => setShow(!show);
  return (
    <Flex flex='1' direction='row' paddingY='3' backgroundColor='black' borderBottomWidth='1' borderColor='#1C232D'>
        <Text color='white' fontSize='14px' fontWeight='bold' flex='1.6' paddingX='1' alignSelf='center'>
          {date}
        </Text>
        <Text color='white' fontSize='14px' fontWeight='bold' flex='1.2' paddingX='1' alignSelf='center'>
          {num}
        </Text>
        <Text color={resultColor? '#37A169' : '#E53E3E'} fontSize='14px' fontWeight='bold' flex='2.5' paddingX='1' alignSelf='center'>
          {result}
        </Text>
        <Box flex='1' justifyContent='center'>
          <DeleteIcon 
            color='red.500'
            flex='1' 
            alignSelf='center'
          />
        </Box>
        
    </Flex>
  );
}
 
export default TableCheck;
