import React, {useState} from "react";
import { Flex, ScrollView, Box } from "native-base";
import TableHeader from "./components/LotteryResults/TableHeader";
import TableBody from "./components/LotteryResults/TableBody";

function Table(){
    const [prize2] = useState([
      { number:'123456', id:1}, { number:'234567', id:2}, { number:'345678', id:3},
      { number:'456789', id:4}, { number:'567890', id:5},
    ]);
    const [prize3] = useState([
      { number:'123456', id:1}, { number:'234567', id:2}, { number:'345678', id:3},
      { number:'456789', id:4}, { number:'567890', id:5}, { number:'123456', id:6},
      { number:'234567', id:7}, { number:'345678', id:8}, { number:'456789', id:9},
      { number:'567890', id:10},
    ])
    const [prize4] = useState([
      { number:'123456', id:1}, { number:'234567', id:2}, { number:'345678', id:3},
      { number:'456789', id:4}, { number:'567890', id:5}, { number:'123456', id:6},
      { number:'234567', id:7}, { number:'345678', id:8}, { number:'456789', id:9},
      { number:'567890', id:10},{ number:'123456', id:11}, { number:'234567', id:12}, 
      { number:'345678', id:13}, { number:'456789', id:14}, { number:'567890', id:15}, 
      { number:'123456', id:16}, { number:'234567', id:17}, { number:'345678', id:18}, 
      { number:'456789', id:19}, { number:'567890', id:20}, { number:'123456', id:21}, 
      { number:'234567', id:22}, { number:'345678', id:23}, { number:'456789', id:24}, 
      { number:'567890', id:25}, { number:'123456', id:26}, { number:'234567', id:27}, 
      { number:'345678', id:28}, { number:'456789', id:29}, { number:'567890', id:30},
      { number:'123456', id:31}, { number:'234567', id:32}, { number:'345678', id:33},
      { number:'456789', id:34}, { number:'567890', id:35}, { number:'123456', id:36},
      { number:'234567', id:37}, { number:'345678', id:38}, { number:'456789', id:39},
      { number:'567890', id:40}, { number:'123456', id:41}, { number:'234567', id:42}, 
      { number:'345678', id:43}, { number:'456789', id:44}, { number:'567890', id:45}, 
      { number:'123456', id:46}, { number:'234567', id:47}, { number:'345678', id:48}, 
      { number:'456789', id:49}, { number:'567890', id:50},
    ])
    const [prize5] = useState([
      { number:'123456', id:1}, { number:'234567', id:2}, { number:'345678', id:3},
      { number:'456789', id:4}, { number:'567890', id:5}, { number:'123456', id:6},
      { number:'234567', id:7}, { number:'345678', id:8}, { number:'456789', id:9},
      { number:'567890', id:10},{ number:'123456', id:11}, { number:'234567', id:12}, 
      { number:'345678', id:13}, { number:'456789', id:14}, { number:'567890', id:15}, 
      { number:'123456', id:16}, { number:'234567', id:17}, { number:'345678', id:18}, 
      { number:'456789', id:19}, { number:'567890', id:20}, { number:'123456', id:21}, 
      { number:'234567', id:22}, { number:'345678', id:23}, { number:'456789', id:24}, 
      { number:'567890', id:25}, { number:'123456', id:26}, { number:'234567', id:27}, 
      { number:'345678', id:28}, { number:'456789', id:29}, { number:'567890', id:30},
      { number:'123456', id:31}, { number:'234567', id:32}, { number:'345678', id:33},
      { number:'456789', id:34}, { number:'567890', id:35}, { number:'123456', id:36},
      { number:'234567', id:37}, { number:'345678', id:38}, { number:'456789', id:39},
      { number:'567890', id:40}, { number:'123456', id:41}, { number:'234567', id:42}, 
      { number:'345678', id:43}, { number:'456789', id:44}, { number:'567890', id:45}, 
      { number:'123456', id:46}, { number:'234567', id:47}, { number:'345678', id:48}, 
      { number:'456789', id:49}, { number:'567890', id:50},{ number:'123456', id:51}, 
      { number:'234567', id:52}, { number:'345678', id:53}, { number:'456789', id:54}, 
      { number:'567890', id:55}, { number:'123456', id:56}, { number:'234567', id:57}, 
      { number:'345678', id:58}, { number:'456789', id:59}, { number:'567890', id:60},
      { number:'123456', id:61}, { number:'234567', id:62}, { number:'345678', id:63},
      { number:'456789', id:64}, { number:'567890', id:65}, { number:'123456', id:66},
      { number:'234567', id:67}, { number:'345678', id:68}, { number:'456789', id:69},
      { number:'567890', id:70},{ number:'123456', id:71}, { number:'234567', id:72}, 
      { number:'345678', id:73}, { number:'456789', id:74}, { number:'567890', id:75}, 
      { number:'123456', id:76}, { number:'234567', id:77}, { number:'345678', id:78}, 
      { number:'456789', id:79}, { number:'567890', id:80},{ number:'123456', id:81}, 
      { number:'234567', id:82}, { number:'345678', id:83}, { number:'456789', id:84}, 
      { number:'567890', id:85}, { number:'123456', id:86}, { number:'234567', id:87}, 
      { number:'345678', id:88}, { number:'456789', id:89}, { number:'567890', id:90},
      { number:'123456', id:91}, { number:'234567', id:92}, { number:'345678', id:93},
      { number:'456789', id:94}, { number:'567890', id:95}, { number:'123456', id:96},
      { number:'234567', id:97}, { number:'345678', id:98}, { number:'456789', id:99},
      { number:'567890', id:100},
    ])

    return ( 
      <ScrollView background='gray.800'>
        <Flex margin='2' height='100%'>
              <TableHeader 
                leftTitle='1st Prize' 
                rightTitle='Last 2 digit' 
                twoColumn={true} />
              <TableBody 
                leftNum='123456' 
                rightNum='42' 
                twoColumn={true}
                bold={true}
                largeFont={true} />

              <TableHeader 
                leftTitle='3 page numbers' 
                rightTitle='Last 3 digits' 
                twoColumn={true} />
              <Flex direction="row">
                <TableBody 
                  leftNum='123' 
                  rightNum='456' 
                  twoColumn={true} 
                  width='50%'
                  alignLeftRight={true}
                  bold={false}
                  largeFont={true}/>
                <TableBody 
                  leftNum='123' 
                  rightNum='456' 
                  twoColumn={true} 
                  width='50%'
                  alignLeftRight={true}
                  bold={false}
                  largeFont={true}/>
              </Flex>

              <TableHeader 
                title='Side Prize 1st Prize'  
                twoColumn={false}/>
              <TableBody 
                leftNum='123456' 
                rightNum='123456' 
                twoColumn={true}
                alignLeftRight={true}
                bold={false}
                largeFont={false} />

              <TableHeader 
                title='2nd Prize'  
                twoColumn={false}/>
              <TableBody 
                twoColumn={false}
                number={
                  prize2.map((prize2) => (
                    <Box 
                      key={prize2.id}
                      width='30%'
                      _text={{
                        color: 'black',
                        textAlign: 'center',
                        fontSize: '20',
                        fontWeight: '600',
                      }}
                    >
                      {prize2.number}
                    </Box>
                  ))
                }/>

              <TableHeader 
                title='3rd Prize'  
                twoColumn={false}/>
              <TableBody 
                twoColumn={false}
                number={
                  prize3.map((prize3) => (
                    <Box 
                      key={prize3.id}
                      width='30%'
                      _text={{
                        color: 'black',
                        textAlign: 'center',
                        fontSize: '20',
                        fontWeight: '600',
                      }}
                    >
                      {prize3.number}
                    </Box>
                  ))
                }
              />

              <TableHeader 
                title='4th Prize'  
                twoColumn={false}/>
              <TableBody 
                twoColumn={false}
                number={
                  prize4.map((prize4) => (
                    <Box 
                      key={prize4.id}
                      width='30%'
                      _text={{
                        color: 'black',
                        textAlign: 'center',
                        fontSize: '20',
                        fontWeight: '600',
                      }}
                    >
                      {prize4.number}
                    </Box>
                  ))
                }/>

              <TableHeader 
                title='5th Prize'  
                twoColumn={false}/>
              <TableBody 
                twoColumn={false}
                number={
                  prize5.map((prize5) => (
                    <Box 
                      key={prize5.id}
                      width='30%'
                      _text={{
                        color: 'black',
                        textAlign: 'center',
                        fontSize: '20',
                        fontWeight: '600',
                      }}
                    >
                      {prize5.number}
                    </Box>
                  ))
                }/>
          </Flex>
        </ScrollView>
  );
}
 
export default Table;