import React from "react";
import {  Flex, Text, Input, Button, HStack, ScrollView} from "native-base";
import TableCheck from "./components/CheckTicket/TableCheck";
 
export default function CheckTicket() {
  // const [show, setShow] = React.useState(false);
  // const handleClick = () => setShow(!show);
  
  return (
      <ScrollView background='gray.800'>
        <Flex 
          direction='column' 
          paddingBottom='3' 
          margin='2' 
          height='100%'
        >
          <HStack
            paddingY='2'
            justifyContent='space-around'
          >
            <Input
              color='white'
              width='75%'
              borderRadius='md'
              placeholder="Search"
              fontSize='md'
            />
            <Button
              backgroundColor='#2B5F75'
              borderRadius='7px'
              _text={{color:'white', fontWeight:'bold', fontSize:'16px'}}
            >
              Search
            </Button>
          </HStack>
          <Text color='gray.500' fontSize='13px' paddingX='2' paddingBottom='2'>
            Enter 3 digit or 6 digit number
          </Text>
  
          <Flex flex='1' direction='row' paddingY='3' backgroundColor='#4A5567'>
            <Text color='white' fontSize='13px' fontWeight='bold' flex='1.6' paddingX='1'>
              DATE
            </Text>
            <Text color='white' fontSize='13px' fontWeight='bold' flex='1.2' paddingX='1'>
              NUMBER
            </Text>
            <Text color='white' fontSize='13px' fontWeight='bold' flex='2.5' paddingX='1'>
              RESULT
            </Text>
            <Text color='white' fontSize='13px' fontWeight='bold' flex='1' paddingX='1'>
              ACTION
            </Text>
          </Flex>
  
          <TableCheck resultColor={true} date='2022-08-16' num='919628' result='3rd prize' action='ACTION'/>
          <TableCheck resultColor={false} date='2022-08-16' num='999' result='Lose' action='ACTION'/>
          <TableCheck resultColor={true} date='2022-08-16' num='331584' result='Side prize 1st prize' action='ACTION'/>
          <TableCheck resultColor={true} date='2022-08-16' num='284' result='3 page numbers' action='ACTION'/>
          <TableCheck resultColor={true} date='2022-08-16' num='331583' result='1st prize' action='ACTION'/>
          <TableCheck resultColor={false} date='2022-08-16' num='122347' result='Lose' action='ACTION'/>
          <TableCheck resultColor={false} date='2022-08-16' num='000' result='Lose' action='ACTION'/>
          <TableCheck resultColor={false} date='2022-08-16' num='989899' result='Lose' action='ACTION'/>
          <TableCheck resultColor={true} date='2022-08-16' num='295954' result='5th prize' action='ACTION'/>
          <TableCheck resultColor={false} date='2022-08-16' num='123' result='Lose' action='ACTION'/>
          
          <Button
            marginY='2.5%'
            backgroundColor='#E53E3E'
            _text={{fontWeight:'bold', fontSize:'16px'}}
          >
            Clear All
          </Button>
        
        </Flex>
      </ ScrollView>
  );
}
