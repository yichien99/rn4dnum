import React, {useState, useEffect} from "react";
import { Box, Flex, Select, CheckIcon } from "native-base";

const Header = () => {
    let [date, setDate] = useState("");
    let [language, setLanguage] = useState("English");

    useEffect(() => {
        let today = new Date();
        let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        setDate(date);
      }, []);

    return (
        <Flex 
            direction="row" 
            padding='2' 
            justifyContent='space-evenly' 
            background='black'
            alignItems='center'
        >
            <Box
                size='10'
                background='#3C3881'
                borderColor='white'
                borderRadius='xl'
                borderWidth='2'
                _text={{
                    color: 'white',
                    fontWeight: 'bold',
                    textAlign:'center',
                    paddingTop:'1.5'
                }}
            >Num</Box>
            <Box >
                <Select 
                    color='white'
                    fontSize='md'
                    selectedValue={date} 
                    minWidth="150" 
                    accessibilityLabel={date} 
                    placeholder={date} 
                    _selectedItem={{
                        bg: "teal.600",
                        endIcon: <CheckIcon size="5" />
                    }} 
                    onValueChange={itemValue => setDate(itemValue)}
                >
                    <Select.Item 
                        label={ new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+new Date().getDate() } 
                        value={ new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+new Date().getDate() }  
                    />
                    <Select.Item 
                        label={ new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+(new Date().getDate()-1) } 
                        value={ new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+(new Date().getDate()-1) }  
                    />
                    <Select.Item 
                        label={ new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+(new Date().getDate()-2) } 
                        value={ new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+(new Date().getDate()-2) }  
                    />
                    <Select.Item 
                        label={ new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+(new Date().getDate()-3) } 
                        value={ new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+(new Date().getDate()-3) }  
                    />
                    <Select.Item 
                        label={ new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+(new Date().getDate()-4) } 
                        value={ new Date().getFullYear()+'-'+(new Date().getMonth()+1)+'-'+(new Date().getDate()-4) }  
                    />
                </Select>
            </Box>
            <Box>
                <Select 
                    color='white'
                    fontSize='md'
                    selectedValue={language} 
                    minWidth="150" 
                    accessibilityLabel={language} 
                    placeholder={language} 
                    _selectedItem={{
                        bg: "teal.600",
                        endIcon: <CheckIcon size="5" />
                    }}
                    onValueChange={itemValue => setLanguage(itemValue)}
                >
                    <Select.Item 
                        label='English' 
                        value='English'  
                    />
                    <Select.Item 
                        label='Chinese' 
                        value='Chinese'  
                    />
                    <Select.Item 
                        label='Thai' 
                        value='Thai'  
                    />
                </Select>
            </Box>
        </Flex>
    );
  };
 
export default Header;