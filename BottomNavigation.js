import { useNavigation } from "@react-navigation/native";
import { Box, Center, HStack, Pressable, Text, Flex } from "native-base";
import React, { useState } from "react";

const BottomNavigation = () => {
    const navigation = useNavigation();
    const [selected, setSelected]=useState(0);
    return (
        <Flex 
            direction="row" 
            background='black' 
            width='100%'
            justifyContent='center'
        >
            <Box flex={1} width="100%" alignSelf="center">
                <HStack alignItems="center" >
                    <Pressable 
                        cursor="pointer" 
                        background={selected === 0 ? '#3C3881' : '#2F8695'} 
                        py="3" 
                        flex={1} 
                        onPress={() => {
                            setSelected(0);
                            navigation.navigate('LotteryResults')
                        }}
                    >
                        <Center>
                            <Text 
                                color='white'
                                fontSize="md"
                                fontWeight="bold"
                            >Lottery results</Text>
                        </Center>
                    </Pressable>
                    <Pressable 
                        cursor="pointer" 
                        background={selected === 1 ? '#3C3881' : '#2F8695'} 
                        py="3" 
                        flex={1} 
                        onPress={() => {
                            setSelected(1);
                            navigation.navigate('CheckTicket')
                        }}
                    >
                        <Center>
                            <Text 
                                color='white'
                                fontSize="md"
                                fontWeight="bold"
                            >Check Ticket</Text>
                        </Center>
                    </Pressable>
                    <Pressable 
                        cursor="pointer" 
                        background={selected === 2 ? '#3C3881' : '#2F8695'} 
                        py="3" 
                        flex={1} 
                        onPress={() => {
                            setSelected(2);
                            navigation.navigate('Statistics')
                        }}
                    >
                        <Center>
                            <Text 
                                color='white'
                                fontSize="md"
                                fontWeight="bold"
                            >Statistics</Text>
                        </Center>
                    </Pressable>
                </HStack>
            </Box>
        </Flex>
    )
  };
 
export default BottomNavigation;